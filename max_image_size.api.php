<?php

/**
 * @file
 * Hooks provided by the Max Image Size module.
 */

/**
 * Allow modules to react to an image being resized.
 *
 * @param array $data
 *   An array with the following keys:
 *   - file: The Drupal file object
 *   - image: The resized image object
 *   - original: The original image object
 *   - max_dimensions: The maximum image dimensions allowed
 *
 * @see max_image_size_resize_file()
 */
function hook_max_image_size_post_resize($data) {}
